package chess;

import java.util.Scanner;
/**
 *Class to create and run a functional chess board.
 *@author Benjamin Cahnbley
 *@author Richard Stoltzfus
*/
public class Chess {

    //String 2D array representing the board that chess is being played on
    public static String[][] board = new String[8][8];

    //boolean arrays checking if corresponding piece has ever moved
    //first 2 for white side, second 2 for black side
    public static boolean[] rookMoved = new boolean[4];

    //first is white side, second is black side
    public static boolean[] kingMoved = new boolean[2];

    //first 8 pawns for white side, second 8 for black side
    //checks if specified pawn has moved the most recent turn
    public static boolean[] pawnMoved = new boolean[16];
    
    public static String kingLocation;
    
    public static String compromisedBy;

    /**
     * Method to create initial chess board
     */
    public static void initialize() {
        // first character represents the cool/side: b = black, w = white
        //second Character represents piece type: R = rook, N = knight, B = bishop, Q = queen, K = king, p = pawn

        board[0][0] = "bR";
        board[0][1] = "bN";
        board[0][2] = "bB";
        board[0][3] = "bQ";
        board[0][4] = "bK";
        board[0][5] = "bB";
        board[0][6] = "bN";
        board[0][7] = "bR";

        board[7][0] = "wR";
        board[7][1] = "wN";
        board[7][2] = "wB";
        board[7][3] = "wQ";
        board[7][4] = "wK";
        board[7][5] = "wB";
        board[7][6] = "wN";
        board[7][7] = "wR";

        board[1][0] = "bp";
        board[1][1] = "bp";
        board[1][2] = "bp";
        board[1][3] = "bp";
        board[1][4] = "bp";
        board[1][5] = "bp";
        board[1][6] = "bp";
        board[1][7] = "bp";

        board[6][0] = "wp";
        board[6][1] = "wp";
        board[6][2] = "wp";
        board[6][3] = "wp";
        board[6][4] = "wp";
        board[6][5] = "wp";
        board[6][6] = "wp";
        board[6][7] = "wp";

        for(int i = 2; i <= 5; i++) {
            for(int j = 0; j < 8; j++) {
                if((i+j)%2 == 0) {
                    board[i][j] = "  ";
                }
                else {
                    board[i][j] = "##";
                }
            }
        }

        for(int i = 0; i < 4; i++){
            rookMoved[i] = false;
        }

        // 16 pawns, 2 checks
        //check1:check to see if it has moved previously to see if double move is doable
        //check2: check if pawn moved once and can be taken using en passant
        for (int i =0; i < 16; i++){
            pawnMoved[i] = false;
        }
    }

    /**
     * Method used to check diagonals if a bishop or Queen is in a direct path from opposite side's king.
     * @param i this is the horizontal number for the location of king.
     * @param j this is the vertical number for the location of king.
     * @param playerATurn this is a boolean which sees if it is player A's turn.
     * @return boolean value of whether or not queen or bishop in a direct diagonal path of king.
     */
    public static boolean checkDiag(int i, int j,boolean playerATurn){
        //check diagonal side of i+,j+
        for(int x = 1; x < 8-i && x+j < 8; x++){
            if((x+j) > 8)
                break;
            if((board[i+x][j+x].equals("bQ") || board[i+x][j+x].equals("bB")) && playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j+x));
            	return true;
            }
            else if((board[i+x][j+x].equals("wQ") || board[i+x][j+x].equals("wB")
            		|| board[i+1][j+1].equals("wp")) && !playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j+x));
                return true;
            }
            else if (!(board[i+x][j+x].equals("  ") || board[i+x][j+x].equals("##"))) 
            	break;
        }

        //check diagonal side of i+,j-
        for(int x = 1; x < 8-i; x++){
            if((j-x) < 0)
                break;
            if((board[i+x][j-x].equals("bQ") || board[i+x][j-x].equals("bB")) && playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j-x));
            	return true;
            }
            else if((board[i+x][j-x].equals("wQ") || board[i+x][j-x].equals("wB")
            		|| board[i+1][j-1].equals("wp")) && !playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j-x));
            	return true;
            }
            else if (!(board[i+x][j-x].equals("  ") || board[i+x][j-x].equals("##"))) 
            	break;
        }

        //check diagonal side of i-,j+
        for(int x = 1; x <= i && x+j < 8; x++){
            if((board[i-x][j+x].equals("bQ") || board[i-x][j+x].equals("bB")
            		|| board[i-1][j+1].equals("bp")) && playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j+x));
            	return true;
            }
            else if((board[i-x][j+x].equals("wQ") || board[i-x][j+x].equals("wB")) && !playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j+x));
            	return true;
            }
            else if (!(board[i-x][j+x].equals("  ") || board[i-x][j+x].equals("##"))) 
            	break;
        }

        //check diagonal side of i-,j-
        for(int x = 1; x <= i; x++){
            if((j-x) < 0)
                break;
            if((board[i-x][j-x].equals("bQ") || board[i-x][j-x].equals("bB")
            		|| board[i-1][j-1].equals("bp")) && playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j-x));
            	return true;
            }
            else if((board[i-x][j-x].equals("wQ") || board[i-x][j-x].equals("wB")) && !playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j-x));
            	return true;
            }
            else if (!(board[i-x][j-x].equals("  ") || board[i-x][j-x].equals("##"))) 
            	break;
        }
        return false;
    }

    /**
     * Method that checks straight sides whether a bishop of queen is in the direct path of opposite side's king.
     * This is a helper method for isCheckMate and kingConflict.
     * @param i this is the horizontal number for the location of king.
     * @param j this is the vertical number for the location of king.
     * @param playerATurn this is a boolean which sees if it is player A's turn.
     * @return boolean value of whether or not queen or rook in a direct straight path of king.
     */
    public static boolean checkStraight(int i, int j, boolean playerATurn){
        //check horizontal line going right
        for ( int x = 1; x < 8-j; x++){
            if((board[i][j+x].equals("bQ") || board[i][j+x].equals("bR")) && playerATurn) {
            	compromisedBy = Integer.toString(i).concat(Integer.toString(j+x));
            	return true;
            }
            else if((board[i][j+x].equals("wQ") || board[i][j+x].equals("wR")) && !playerATurn) {
            	compromisedBy = Integer.toString(i).concat(Integer.toString(j+x));
            	return true;
            }
            //check if a piece is potentially blocking a check
            else if (!(board[i][j+x].equals("  ") || board[i][j+x].equals("##"))) 
            	break;
        }

        //check horizontal line going left
        for ( int x = 1; x <= j; x++){
            if((board[i][j-x].equals("bQ") || board[i][j-x].equals("bR")) && playerATurn) {
            	compromisedBy = Integer.toString(i).concat(Integer.toString(j-x));
            	return true;
            }
            else if((board[i][j-x].equals("wQ") || board[i][j-x].equals("wR")) && !playerATurn) {
            	compromisedBy = Integer.toString(i).concat(Integer.toString(j-x));
            	return true;
            }
            //check if a piece is potentially blocking a check
            else if (!(board[i][j-x].equals("  ") || board[i][j-x].equals("##"))) 
            	break;
        }

        //check vertical line going down
        for ( int x = 1; x < 8-i; x++){
            if((board[i+x][j].equals("bQ") || board[i+x][j].equals("bR")) && playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j));
            	return true;
            }
            else if((board[i+x][j].equals("wQ") || board[i+x][j].equals("wR")) && !playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j));
            	return true;
            }
            //check if a piece is potentially blocking a check
            else if (!(board[i+x][j].equals("  ") || board[i+x][j].equals("##"))) 
            	break;
        }

        //check vertical line going up
        for ( int x = 1; x <= i; x++){
            if((board[i-x][j].equals("bQ") || board[i-x][j].equals("bR")) && playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j));
            	return true;
            }
            else if((board[i-x][j].equals("wQ") || board[i-x][j].equals("wR")) && !playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j));
            	return true;
            }
            //check if a piece is potentially blocking a check
            else if (!(board[i-x][j].equals("  ") || board[i-x][j].equals("##"))) 
            	break;
        }
        return false;
    }

    /**
     * Method that checks if the string from scanner is valid as well as the move being valid.
     * The move is valid if it picks a valid piece, moves to a location that is valid for that piece type
     * and it moves to a location not held by one of its own team's pieces.
     * @param input String input from scanner
     * @param playerATurn boolean to see if it is Player A's turn
     * @return boolean value on whether or not this turn is valid
     */
    public static boolean isValid(String input, boolean playerATurn) {

        if(((int)input.charAt(0) >= 65 && (int)input.charAt(0) <= 72) ||((int)input.charAt(0) >= 97 && (int)input.charAt(0) <= 104)){
            if(((int)input.charAt(3) >= 65 && (int)input.charAt(3) <= 72) ||((int)input.charAt(3) >= 97 && (int)input.charAt(3) <= 104)){
                if(((int)input.charAt(1) >= 48 && (int)input.charAt(1) <= 56) && ((int)input.charAt(4) >= 48 && (int)input.charAt(4) <= 56)){
                    int startB = (int)Character.toLowerCase(input.charAt(0)) - 97;
                    int startA = 8 - ((int)input.charAt(1) - 48);
                    int endB = (int)Character.toLowerCase(input.charAt(3)) - 97;
                    int endA = 8 - ((int)input.charAt(4) - 48);

                    //correct player moving piece
                    if((board[endA][endB].charAt(0) == 'w' && playerATurn) ||(board[endA][endB].charAt(0) == 'b' && !playerATurn))
                        return false;

                    if((board[startA][startB].charAt(0) != 'w' && playerATurn) || (board[startA][startB].charAt(0) != 'b' && !playerATurn))
                        return false;

                    //pawn movement
                    if(board[startA][startB].charAt(1) == 'p'){
                        //check if its a move
                        if(startB == endB){
                            //check if normal move
                            if(Math.abs(startA - endA) == 1 && (board[endA][endB].equals("  ") || board[endA][endB].equals("##"))){
                                return((playerATurn && endA+1 == startA) || (!playerATurn && startA+1 == endA));
                            }

                            //check if its an initial double move
                            else if(Math.abs(startA - endA) == 2 && (board[endA][endB].equals("  ") || board[endA][endB].equals("##"))){
                                return ((playerATurn && startA == 6 && endA+2 == startA) || (!playerATurn && startA == 1 && endA == startA+2));
                            }
                            else
                                return false;
                        }

                        else if(Math.abs(startA - endA) == 1 && Math.abs(startB - endB) == 1){
                            //check if normal overtake
                            if((playerATurn && endA+1 == startA && board[endA][endB].charAt(0) == 'b')
                            		|| (!playerATurn && endA == startA+1 && board[endA][endB].charAt(0) == 'w'))
                                return true;

                            //check if en passant overtake
                            else if((playerATurn && endA+1 == startA && board[endA+1][endB].charAt(0) == 'b' && pawnMoved[8+endB] && endA == 2)
                            		|| (!playerATurn && endA == startA+1 && board[endA-1][endB].charAt(0) == 'w'  && pawnMoved[endB] && endA == 5)){
                                return true;
                            }
                            else
                                return false;
                        }
                        else
                            return false;

                    }
                    //rook movement
                    else if(board[startA][startB].charAt(1) == 'R'){
                    	//vertical
                    	if(startA != endA && startB == endB){
                    		if(endA > startA){
                    			for(int i = startA+1; i < endA; i++){
                    				if(!board[i][startB].equals("  ") && !board[i][startB].equals("##"))
                    					return false;
                    			}
                    		}
                    		else{
                    			for(int i = startA-1; i > endA; i--){
                    				if(!board[i][startB].equals("  ") && !board[i][startB].equals("##"))
                    					return false;
                    			}
                    		}
                    		return true;
                    	}
                    	//horizontal
                    	else if(startA == endA && startB != endB){
                    		if(endB > startB){
                    			for(int i = startB+1; i < endB; i++){
                    				if(!board[startA][i].equals("  ") && !board[startA][i].equals("##"))
                    					return false;
                    			}
                    		}
                    		else{
                    			for(int i = startB-1; i > endB; i--){
                    				if(!board[startA][i].equals("  ") && !board[startA][i].equals("##"))
                    					return false;
                    			}
                    		}
                    		return true;
                    	}
                    }
                    //knight movement
                    else if(board[startA][startB].charAt(1) == 'N'){
                        if((Math.abs(startA - endA) == 1 && Math.abs(startB - endB) == 2) || (Math.abs(startA - endA) == 2 && Math.abs(startB - endB) == 1))
                            return true;
                        else
                            return false;
                    }
                    //bishop movement
                    else if(board[startA][startB].charAt(1) == 'B'){
                        if (Math.abs(Math.abs(startA-endA) - Math.abs(startB - endB)) == 0){
                            if(endA > startA && endB > startB){
                                for(int i = 1; i < endA - startA; i++){
                                    if(!board[startA+i][startB+i].equals("  ") && !board[startA+i][startB+i].equals("##"))
                                        return false;
                                }
                                return true;

                            }
                            else if( endA > startA && endB < startB) {
                                for (int i = 1; i < endA - startA; i++) {
                                    if (!board[startA + i][startB - i].equals("  ") && !board[startA + i][startB - i].equals("##"))
                                        return false;
                                }
                                return true;
                            }
                            else if (endA < startA && endB < startB){
                                for (int i = 1; i < startA - endA; i++) {
                                    if (!board[startA - i][startB - i].equals("  ") && !board[startA - i][startB - i].equals("##"))
                                        return false;
                                }
                                return true;
                            }
                            else if (endA < startA && endB > startB){
                                for (int i = 1; i < startA - endA; i++) {
                                    if (!board[startA - i][startB + i].equals("  ") && !board[startA - i][startB + i].equals("##"))
                                        return false;
                                }
                                return true;
                            }
                        }
                        else
                            return false;

                    }
                    //queen movement
                    else if(board[startA][startB].charAt(1) == 'Q'){
                    	//diagonal
                        if (Math.abs(Math.abs(startA-endA) - Math.abs(startB - endB)) == 0){
                            if(endA > startA && endB > startB){
                                for(int i = 1; i < endA - startA; i++){
                                    if(!board[startA+i][startB+i].equals("  ") && !board[startA+i][startB+i].equals("##"))
                                        return false;
                                }
                                return true;
                            }
                            else if( endA > startA && endB < startB) {
                                for (int i = 1; i < endA - startA; i++) {
                                    if (!board[startA + i][startB - i].equals("  ") && !board[startA + i][startB - i].equals("##"))
                                        return false;
                                }
                                return true;
                            }
                            else if (endA < startA && endB < startB){
                                for (int i = 1; i < startA - endA; i++) {
                                    if (!board[startA - i][startB - i].equals("  ") && !board[startA - i][startB - i].equals("##"))
                                        return false;
                                }
                                return true;
                            }
                            else if (endA < startA && endB > startB){
                                for (int i = 1; i < startA - endA; i++) {
                                    if (!board[startA - i][startB + i].equals("  ") && !board[startA - i][startB + i].equals("##"))
                                        return false;
                                }
                                return true;
                            }
                        }
                        //vertical
                        else if (startA != endA && startB == endB){
                            if(endA > startA){
                                for(int i = startA+1; i < endA; i++){
                                    if(!board[i][startB].equals("  ") && !board[i][startB].equals("##"))
                                        return false;
                                }
                            }
                            else{
                                for(int i = startA-1; i > endA; i--){
                                    if(!board[i][startB].equals("  ") && !board[i][startB].equals("##")) {
                                        return false;
                                    }
                                }
                            }
                            return true;
                        }
                        //horizontal
                        else if (startA == endA && startB != endB){
                            if(endB > startB){
                                for(int i = startB+1; i < endB; i++){
                                    if(!board[startA][i].equals("  ") && !board[startA][i].equals("##"))
                                        return false;
                                }
                            }
                            else{
                                for(int i = startB-1; i > endB; i--){
                                    if(!board[startA][i].equals("  ") && !board[startA][i].equals("##")) {
                                        return false;
                                    }
                                }
                            }
                            return true;
                        }
                        else
                            return false;
                    }
                    //king movement
                    else if(board[startA][startB].charAt(1) == 'K'){
                        if(Math.abs(startA-endA) <= 1 && Math.abs(startB-endB) <= 1)
                            return true;
                        else if (startB-endB == 2 && playerATurn && !rookMoved[0] && !kingMoved[0]){
                            for(int i = 1; i < 3; i++) {
                                if(!board[startA][startB-i].equals("  ") && !board[startA][startB-i].equals("##"))
                                    return false;
                            }
                            return true;
                        }
                        else if (startB-endB == -2 && playerATurn && !rookMoved[1] && !kingMoved[0]){
                            for(int i = 1; i < 3; i++) {
                                if(!board[startA][startB+i].equals("  ") && !board[startA][startB+i].equals("##"))
                                    return false;
                            }
                            return true;
                        }
                        else if (startB-endB == 2 && !playerATurn && !rookMoved[2] && !kingMoved[1]){
                            for(int i = 1; i < 3; i++) {
                                if(!board[startA][startB-i].equals("  ") && !board[startA][startB-i].equals("##"))
                                    return false;
                            }
                            return true;
                        }
                        else if (startB-endB == -2 && !playerATurn && !rookMoved[3] && !kingMoved[1]){
                            for(int i = 1; i < 3; i++) {
                                if(!board[startA][startB+i].equals("  ") && !board[startA][startB+i].equals("##"))
                                    return false;
                            }
                            return true;
                        }
                    }
                }
            }
        }
    return false;
    }

    /**
     * Method to checks if an opponent's knight is one move away from their king
     * @param i this is the horizontal number for the location of king.
     * @param j this is the vertical number for the location of king.
     * @param playerATurn this is a boolean which sees if it is player A's turn.
     * @return boolean value of whether or not a knight in one move from king.
     */
    public static boolean checkKnight(int i, int j, boolean playerATurn){
        if(i+1 <8 && j+2<8 && board[i+1][j+2].equals("wN") && !playerATurn)
            return true;
        else if (i+1 < 8 && j-2 >= 0 && board[i+1][j-2].equals("wN") && !playerATurn)
            return true;
        else if (i-1 >= 0 && j-2 >= 0 && board[i-1][j-2].equals("wN") && !playerATurn)
            return true;
        else if (i-1 >= 0 && j+2 < 8 && board[i-1][j+2].equals("wN") && !playerATurn)
            return true;
        else if (i+2 < 8 && j+1 < 8 && board[i+2][j+1].equals("wN") && !playerATurn)
            return true;
        else if (i-2 >= 0 && j+1 < 8 && board[i-2][j+1].equals("wN") && !playerATurn)
            return true;
        else if (i+2 < 8 && j-1 >= 0 && board[i+2][j-1].equals("wN") && !playerATurn)
            return true;
        else if (i-2>= 0 && j-1 >= 0 && board[i-2][j-1].equals("wN") && !playerATurn)
            return true;

        if(i+1 <8 && j+2<8 && board[i+1][j+2].equals("bN") && playerATurn)
            return true;
        else if (i+1 < 8 && j-2 >= 0 && board[i+1][j-2].equals("bN") && playerATurn)
            return true;
        else if (i-1 >= 0 && j-2 >= 0 && board[i-1][j-2].equals("bN") && playerATurn)
            return true;
        else if (i-1 >= 0 && j+2 < 8 && board[i-1][j+2].equals("bN") && playerATurn)
            return true;
        else if (i+2 < 8 && j+1 < 8 && board[i+2][j+1].equals("bN") && playerATurn)
            return true;
        else if (i-2 >= 0 && j+1 < 8 &&board[i-2][j+1].equals("bN") && playerATurn)
            return true;
        else if (i+2 < 8 && j-1 >= 0 && board[i+2][j-1].equals("bN") && playerATurn)
            return true;
        else if (i-2>= 0 && j-1 >= 0 && board[i-2][j-1].equals("bN") && playerATurn)
            return true;

        return false;

    }

    /**
     * Method that checks whether the king is in current danger from an enemy unit
     * @param input String from scanner that shows the current player's move
     * @param playerATurn his is a boolean which sees if it is player A's turn.
     * @return boolean value on whether there is a conflict or not
     */
    public static boolean kingConflict(String input, boolean playerATurn) {
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if (board[i][j].equals("bK") && !playerATurn){
                	kingLocation = Integer.toString(i).concat(Integer.toString(j));
                	boolean diag = checkDiag(i, j, playerATurn);
                	boolean str = checkStraight(i, j, playerATurn);
                	boolean knight = checkKnight(i, j, playerATurn);
                	return (diag || str || knight);
                }
                if (board[i][j].equals("wK") && playerATurn){
                	kingLocation = Integer.toString(i).concat(Integer.toString(j));
                	boolean diag = checkDiag(i, j, playerATurn);
                	boolean str = checkStraight(i, j, playerATurn);
                	boolean knight = checkKnight(i, j, playerATurn);
                	return (diag || str || knight);
                }

            }
        }
        return false;
    }
    
    public static boolean isCompromised(int endA, int endB, boolean playerATurn) {
    	boolean diag = checkDiag(endA, endB, playerATurn);
        boolean str = checkStraight(endA, endB, playerATurn);
        boolean knight = checkKnight(endA, endB, playerATurn);
        return (diag || str || knight);
    }
    
    public static boolean isCompromisedBlock(int endA, int endB, boolean playerATurn) {
    	boolean diag = checkDiagBlock(endA, endB, playerATurn);
        boolean str = checkStraightBlock(endA, endB, playerATurn);
        boolean knight = checkKnight(endA, endB, playerATurn);
        return (diag || str || knight);
    }
    
    public static boolean checkDiagBlock(int i, int j,boolean playerATurn){
        //check diagonal side of i+,j+
        for(int x = 1; x < 8-i && x+j < 8; x++){
            if((x+j) > 8)
                break;
            if((board[i+x][j+x].equals("bQ") || board[i+x][j+x].equals("bB")) && playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j+x));
            	return true;
            }
            else if((board[i+x][j+x].equals("wQ") || board[i+x][j+x].equals("wB")) && !playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j+x));
            	return true;
            }
            else if (!(board[i+x][j+x].equals("  ") || board[i+x][j+x].equals("##"))) 
            	break;
        }

        //check diagonal side of i+,j-
        for(int x = 1; x < 8-i; x++){
            if((j-x) < 0)
                break;
            if((board[i+x][j-x].equals("bQ") || board[i+x][j-x].equals("bB")) && playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j-x));
            	return true;
            }
            else if((board[i+x][j-x].equals("wQ") || board[i+x][j-x].equals("wB")) && !playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j-x));
            	return true;
            }
            else if (!(board[i+x][j-x].equals("  ") || board[i+x][j-x].equals("##"))) 
            	break;
        }

        //check diagonal side of i-,j+
        for(int x = 1; x <= i && x+j < 8; x++){
            if((board[i-x][j+x].equals("bQ") || board[i-x][j+x].equals("bB")) && playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j+x));
            	return true;
            }
            else if((board[i-x][j+x].equals("wQ") || board[i-x][j+x].equals("wB")) && !playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j+x));
            	return true;
            }
            else if (!(board[i-x][j+x].equals("  ") || board[i-x][j+x].equals("##"))) 
            	break;
        }

        //check diagonal side of i-,j-
        for(int x = 1; x <= i; x++){
            if((j-x) < 0)
                break;
            if((board[i-x][j-x].equals("bQ") || board[i-x][j-x].equals("bB")) && playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j-x));
            	return true;
            }
            else if((board[i-x][j-x].equals("wQ") || board[i-x][j-x].equals("wB")) && !playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j-x));
            	return true;
            }
            else if (!(board[i-x][j-x].equals("  ") || board[i-x][j-x].equals("##"))) 
            	break;
        }
        return false;
    }

    public static boolean checkStraightBlock(int i, int j, boolean playerATurn){
        //check horizontal line going right
        for ( int x = 1; x < 8-j; x++){
            if((board[i][j+x].equals("bQ") || board[i][j+x].equals("bR")) && playerATurn) {
            	compromisedBy = Integer.toString(i).concat(Integer.toString(j+x));
            	return true;
            }
            else if((board[i][j+x].equals("wQ") || board[i][j+x].equals("wR")) && !playerATurn) {
            	compromisedBy = Integer.toString(i).concat(Integer.toString(j+x));
            	return true;
            }
            //check if a piece is potentially blocking a check
            else if (!(board[i][j+x].equals("  ") || board[i][j+x].equals("##"))) 
            	break;
        }

        //check horizontal line going left
        for ( int x = 1; x <= j; x++){
            if((board[i][j-x].equals("bQ") || board[i][j-x].equals("bR")) && playerATurn) {
            	compromisedBy = Integer.toString(i).concat(Integer.toString(j-x));
            	return true;
            }
            else if((board[i][j-x].equals("wQ") || board[i][j-x].equals("wR")) && !playerATurn) {
            	compromisedBy = Integer.toString(i).concat(Integer.toString(j-x));
            	return true;
            }
            //check if a piece is potentially blocking a check
            else if (!(board[i][j-x].equals("  ") || board[i][j-x].equals("##"))) 
            	break;
        }

        //check vertical line going down
        for ( int x = 1; x < 8-i; x++){
            if((board[i+x][j].equals("bQ") || board[i+x][j].equals("bR")) && playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j));
            	return true;
            }
            else if((board[i+x][j].equals("wQ") || board[i+x][j].equals("wR")
            		|| board[i+1][j].equals("wp") || (i+2 == 6 && board[i+2][j].equals("wp") 
            		&& (board[i+1][j].equals("  ") || board[i+1][j].equals("##")))) && !playerATurn) {
            	compromisedBy = Integer.toString(i+x).concat(Integer.toString(j));
            	return true;
            }
            //check if a piece is potentially blocking a check
            else if (!(board[i+x][j].equals("  ") || board[i+x][j].equals("##"))) 
            	break;
        }

        //check vertical line going up
        for ( int x = 1; x <= i; x++){
            if((board[i-x][j].equals("bQ") || board[i-x][j].equals("bR")
            		|| board[i-1][j].equals("bp") || (i-2 == 1 && board[i-2][j].equals("wp") 
                    && (board[i-1][j].equals("  ") || board[i-1][j].equals("##")))) && playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j));
            	return true;
            }
            else if((board[i-x][j].equals("wQ") || board[i-x][j].equals("wR")) && !playerATurn) {
            	compromisedBy = Integer.toString(i-x).concat(Integer.toString(j));
            	return true;
            }
            //check if a piece is potentially blocking a check
            else if (!(board[i-x][j].equals("  ") || board[i-x][j].equals("##"))) 
            	break;
        }
        return false;
    }
    
    /**
     * Method that checks every turn if a checkmate has occurred
     * @param playerATurn boolean to see if it is player A who is checkmated
     * @return boolean value if checkmate has occurred for the boolean playerATurn
     */
    public static boolean isCheckMate(boolean playerATurn, String input, String piece){
        int endB = ((int)Character.toLowerCase(input.charAt(3)) - 97);
        int endA = 8 - ((int)input.charAt(4) - 48);
        int kingI = kingLocation.charAt(0) - 48;
        int kingJ = kingLocation.charAt(1) - 48;
    	//determine whether offending piece can be taken by another piece
        if (isCompromised(endA, endB, !playerATurn)){
        	int tempA = compromisedBy.charAt(0)-48;
        	int tempB = compromisedBy.charAt(1)-48;
        	String tempPiece = board[tempA][tempB];
        	board[tempA][tempB] = "  ";
        	if (!kingConflict(input, playerATurn)) {
        		board[tempA][tempB] = tempPiece;
        		return false;
        	}
        	else
        		board[tempA][tempB] = tempPiece;
        }
        //determine whether king can take offending piece without moving into check
        if (Math.abs(endA - kingI) == 1
        		|| Math.abs(endB - kingJ) == 1) {
            if (!isCompromised(endA, endB, playerATurn)) {
            	return false;
            }
        }
        //determine whether king can move out of check
        boolean safePlace = false;
        if (kingI - 1 >= 0) {
        	if (kingJ - 1 >= 0) {
        		if (board[kingI-1][kingJ-1].charAt(0) == ' '
        				|| board[kingI-1][kingJ-1].charAt(0) == '#')
        		safePlace = (safePlace || (!isCompromised(kingI-1, kingJ-1, playerATurn)));
        	}
        	if (board[kingI-1][kingJ].charAt(0) == ' '
    				|| board[kingI-1][kingJ].charAt(0) == '#')
        	safePlace = (safePlace || (!isCompromised(kingI-1, kingJ, playerATurn)));
        	if (kingJ + 1 < 8) {
        		if (board[kingI-1][kingJ+1].charAt(0) == ' '
        				|| board[kingI-1][kingJ+1].charAt(0) == '#')
            	safePlace = (safePlace || (!isCompromised(kingI-1, kingJ+1, playerATurn)));
        	}
        }
        if (kingJ - 1 >= 0) {
        	if (board[kingI][kingJ-1].charAt(0) == ' '
    				|| board[kingI][kingJ-1].charAt(0) == '#')
    		safePlace = (safePlace || (!isCompromised(kingI, kingJ-1, playerATurn)));
    		if (kingI + 1 < 8) {
    			if (board[kingI+1][kingJ-1].charAt(0) == ' '
        				|| board[kingI+1][kingJ-1].charAt(0) == '#')
    			safePlace = (safePlace || (!isCompromised(kingI+1, kingJ-1, !playerATurn)));
    		}
    	}
        if (kingI + 1 < 8) {
        	if (board[kingI+1][kingJ].charAt(0) == ' '
    				|| board[kingI+1][kingJ].charAt(0) == '#')
        	safePlace = (safePlace || (!isCompromised(kingI+1, kingJ, playerATurn)));
        	if (kingJ + 1 < 8) {
        		if (board[kingI+1][kingJ+1].charAt(0) == ' '
        				|| board[kingI+1][kingJ+1].charAt(0) == '#')
        		safePlace = (safePlace || (!isCompromised(kingI+1, kingJ+1, playerATurn)));
        	}
        }
        if (kingJ + 1 < 8) {
        	if (board[kingI][kingJ+1].charAt(0) == ' '
    				|| board[kingI][kingJ+1].charAt(0) == '#')
        	safePlace = (safePlace || (!isCompromised(kingI, kingJ+1, playerATurn)));
        }
        if (safePlace) {
        	return false;
        }
        //determine whether any piece can move into a position to block
        if (!(piece.charAt(1) == 'p' || piece.charAt(1) == 'N')) {
        	if (checkMateHelper(piece, endA, endB, kingI, kingJ, playerATurn)) {
        		return false;
        	}
        }
        return true;
    }
    
    public static boolean checkMateHelper(String piece, int endA, int endB,
    		int kingI, int kingJ, boolean playerATurn) {
    	int possibleBlocks = 0;
    	boolean canBlock = false;
    	if (piece.charAt(1) == 'R' || piece.charAt(1) == 'Q') {
    		if (endA == kingI) {
        		possibleBlocks = Math.abs(endB - kingJ);
        		if (endB > kingJ) {
        			for (int j = 1; j < possibleBlocks; j++) {
            			canBlock = (canBlock || (isCompromisedBlock(kingI, kingJ+j, !playerATurn)));
            		}
        		}
        		if (endB < kingJ) {
        			for (int j = 1; j < possibleBlocks; j++) {
        				canBlock = (canBlock || (isCompromisedBlock(kingI, kingJ-j, !playerATurn)));
        			}
        		}
    		}
    		if (endB == kingJ) {
    			possibleBlocks = Math.abs(endA - kingI);
    			if (endA > kingI) {
    				for (int i = 1; i < possibleBlocks; i++) {
            			canBlock = (canBlock || (isCompromisedBlock(kingI+i, kingJ, !playerATurn)));
    				}
    			}
    			if (endA < kingI) {
    				for (int i = 1; i < possibleBlocks; i++) {
            			canBlock = (canBlock || (isCompromisedBlock(kingI-i, kingJ, !playerATurn)));
    				}
    			}
    		}
    	}
    	if (piece.charAt(1) == 'B' || (piece.charAt(1) == 'Q'
    			&& Math.abs(endA - kingI) == Math.abs(endB - kingJ))) {
    		possibleBlocks = Math.abs(endA - kingI);
    		if (endA > kingI) {
    			if (endB > kingJ) {
    				for (int i = 1; i < possibleBlocks; i++) {
    					canBlock = (canBlock || (isCompromisedBlock(kingI+i, kingJ+i, !playerATurn)));
    				}
    			}
    			if (endB < kingJ) {
    				for (int i = 1; i < possibleBlocks; i++) {
    					canBlock = (canBlock || (isCompromisedBlock(kingI+i, kingJ-i, !playerATurn)));
    				}
    			}
    		}
    		if (endA < kingI) {
    			if (endB > kingJ) {
    				for (int i = 1; i < possibleBlocks; i++) {
    					canBlock = (canBlock || (isCompromisedBlock(kingI-i, kingJ+i, !playerATurn)));
    				}
    			}
    			if (endB < kingJ) {
    				for (int i = 1; i < possibleBlocks; i++) {
    					canBlock = (canBlock || (isCompromisedBlock(kingI-i, kingJ-i, !playerATurn)));
    				}
    			}
    		}
    	}
    	return canBlock;
    }

    /**
     * Method that prints the current board
     */
    public static void printBoard() {
        System.out.println();
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println(" " + (8-i));
        }
        for(int i = 0; i < 8; i++) {
            System.out.print(" " + (char) (i+97) + " ");
        }
        System.out.println();
        System.out.println();
    }

    /**
     * Main method where it starts and iterates each turn of chess game
     * @param args Unused
     */
    public static void main(String[] args) {

        boolean check = false;
        boolean gameOver = false;
        boolean playerATurn = true;
        Scanner sc = new Scanner(System.in);
        String draw = "";

        initialize();

        while(!gameOver){
            if(playerATurn){
                for (int i =0; i < 8; i++){
                    pawnMoved[i] = false;
                }
            }
            else{
                for (int i =8; i < 16; i++){
                    pawnMoved[i] = false;
                }
            }
        	
            printBoard();
        	if(playerATurn)
                System.out.print("White's Move: ");
            else
                System.out.print("Black's Move: ");

            String input = sc.nextLine();
            if(input.equals("resign")){
                if(playerATurn)
                    System.out.println("Black wins");
                else
                    System.out.println("White wins");
                sc.close();
                return;
            }
            
/*if (input.equals("dev")) {
	while (!input.equals("udev")) {
		input = sc.nextLine();
		if (input.equals("udev")) {
			printBoard();
			break;
		}
		board[input.charAt(0)-48][input.charAt(1)-48] = sc.nextLine();
		printBoard();
	}
}*/

            if(draw.contentEquals(" draw?"))
                if(input.equals("draw"))
                    break;
            if(input.length() > 5) {
                draw = input.substring(5).replaceAll("\\P{Print}","");
                input = input.substring(0,5);
            }

            //Check input if its a valid move and input
            while((input.length() != 5) || !isValid(input, playerATurn)){
            	if(input.equals("resign")){
                    if(playerATurn)
                        System.out.println("Black wins");
                    else
                        System.out.println("White wins");
                    sc.close();
                    return;
                }
                System.out.println("Invalid Input. Try again");
                if(playerATurn)
                    System.out.print("White's Move: ");
                else
                    System.out.print("Black's Move: ");
                input = sc.nextLine();
            }

            int startB = ((int)Character.toLowerCase(input.charAt(0)) - 97);
            int startA = 8 - ((int)input.charAt(1) - 48);
            int endB = ((int)Character.toLowerCase(input.charAt(3)) - 97);
            int endA = 8 - ((int)input.charAt(4) - 48);

            String piece = board[startA][startB];
            String temp = board[endA][endB];
            board[endA][endB] = piece;
            
            if((startA+startB)%2 == 0)
                board[startA][startB] = "  ";
            else
                board[startA][startB] = "##";

            if(check){
                if(kingConflict(input,playerATurn)){
                    board[endA][endB] = temp;
                    board[startA][startB] = piece;
                    System.out.println("Invalid Input. Try again");
                }
                else {
                    check = false;
                    if (isCheckMate(!playerATurn, input, piece)) {
                    	printBoard();
                    	System.out.println("Checkmate");
                    	if (playerATurn) {
                            System.out.println("White wins");
                    	}
                    	else {
                            System.out.println("Black wins");
                    	}
                    	sc.close();
                    	return;
                    }
                    if (kingConflict(input,!playerATurn) && !isCheckMate(playerATurn,input,piece)) {
                        check = true;
                        System.out.println("Check");
                    }
                    playerATurn = !playerATurn;
                }
            }
            else if(kingConflict(input,playerATurn)){
                board[endA][endB] = temp;
                board[startA][startB] = piece;
                System.out.println("Invalid Input. Try again");
            }
            else{
                if(piece.charAt(1) == 'p'){
                    if (Math.abs(startB-endB) == 1 && ((board[endA][endB] == "wp" 
                    		&& playerATurn) || (board[endA][endB] == "bp" && !playerATurn))){
                        if((startA+startB)%2 == 1)
                            board[startA][endB] = "  ";
                        else
                            board[startA][endB] = "##";
                        
                    }
                    else if (Math.abs(startA-endA) == 2 && playerATurn)
                        pawnMoved[startB] = true;
                    else if (Math.abs(startA-endA) == 2 && !playerATurn)
                        pawnMoved[startB+8] = true;
                    else if ((endA == 0 && playerATurn) || (endA == 7 && !playerATurn)){
                        if(!draw.equals(" draw?")){
                        	char pawnType = 'Q';
                        	if (!draw.equals("")) {
                        		pawnType = draw.charAt(draw.length()-1);
                        	}
                        	if (playerATurn){
                        		piece = "w" + pawnType;
                        	}
                        	else
                        		piece = "b" + pawnType;
                        }
                    }
                }
                else if(input.substring(0,2).equals("a1"))
                    rookMoved[0] = true;
                else if(input.substring(0,2).equals("h1"))
                    rookMoved[1] = true;
                else if(input.substring(0,2).equals("a8"))
                    rookMoved[2] = true;
                else if (input.substring(0,2).equals("h8"))
                    rookMoved[3] = true;
                else if (piece.charAt(1) == 'K' && playerATurn) {
                	kingMoved[0] = true;
                    if (endB == 2) {
                    	board[7][0] = "##";
                    	board[7][3] = "wR";
                    	rookMoved[0] = true;
                    }
                    else if (endB == 6) {
                    	board[7][7] = "  ";
                    	board[7][5] = "wR";
                    	rookMoved[1] = true;
                    }
                }
                else if (piece.charAt(1) == 'K' && !playerATurn) {
                    kingMoved[1] = true;
                    if (endB == 2) {
                    	board[0][0] = "  ";
                    	board[0][3] = "bR";
                    	rookMoved[2] = true;
                    }
                    else if (endB == 6) {
                    	board[0][7] = "##";
                    	board[0][5] = "bR";
                    	rookMoved[3] = true;
                    }
                }
                board[endA][endB] = piece;

                if (kingConflict(input,!playerATurn)) {

                    if (isCheckMate(!playerATurn, input, piece)) {
                    	gameOver = true;
                    	System.out.println("Checkmate");
                    	if (playerATurn) {
                            System.out.println("White wins");
                    	}
                    	else {
                            System.out.println("Black wins");
                    	}
                    	sc.close();
                    	return;
                    }
                    else{
                        check = true;
                        System.out.println("Check");
                    }
                }
                playerATurn = !playerATurn;
            }
        }
        sc.close();
    }
}